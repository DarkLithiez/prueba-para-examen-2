//1ºEjercicio a)
public class Block {
	private int[] numbers;
	//1ºEjercicio I)
	public Block(int n){
		boolean repeated;
		int randomNumber=0;
		numbers= new int[n];
		if (n<6){
			n=6;
		}
		if (n>49){
			n=49;
		}
		for(int i=0;i<n;i++){
			repeated=true;
			while(repeated){
				randomNumber=(int)(Math.random()*49)+1;
				repeated=false;
				for (int j=0;j<i;j++){
					if (numbers[i]==randomNumber){
						repeated=false;
					}
				}
			}
			numbers[i]=randomNumber;
		}
	}
	//1ºEjercicio II)
	public Block(int [] num){
		numbers=num;
	}
	//1ºEjercicio III)
	public	String toString(){
		String s;
		s=""+numbers[0];
		for(int i=0;i<numbers.length;i++){
			s=s+","+numbers[i];
			}
		return s;
	}
	//1ºEjercicio IV)
	public int[] getNumbers(){
		return numbers;
		}
	
}

